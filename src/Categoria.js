import React, { Component } from 'react'
import {Link} from 'react-router-dom'

class Categoria extends Component {
    constructor(props) {
        super(props)

        this.state = {
            produtos: [],
            categoriaAtual: [],
            id: null
        }

        this.loadData = this.loadData.bind(this);
        this.renderProduto = this.renderProduto.bind(this);
    }

    componentDidMount() {
        const catID = this.props.match.params.catID;
        this.loadData(catID);
    }

    componentWillReceiveProps(newProps) {
        if (newProps.match.params.catID !== this.state.id) {
            const catID = newProps.match.params.catID;
            this.loadData(catID);
        }
    }

    loadData(catID) {
        this.setState({ id: catID })
        this.props.loadProdutosCategorias(catID)
        this.props.loadCategoriaAtual(catID)
    }

    renderProduto(prod) {
        return (
            <p key={prod.id} className="alert alert-info">
                <button className="btn btn-danger" onClick={() => this.props.removeProduto(prod)}>
                    <i className="glyphicon glyphicon-remove"></i>
                </button>

                <Link to={'/produtos/editar/' + prod.id}>[ Editar ]</Link>

                {prod.produto}
            </p>
        )
    }

    render() {
        return (
            <div>
                <h2>Categoria atual: {this.props.match.params.catID}</h2>
                <h3>{ this.props.categoriaAtual && this.props.categoriaAtual.categoria}</h3>

                {this.props.produtos.length === 0 &&
                    <div>Nenhum registro encontrado</div>
                }

                {this.props.produtos.map(this.renderProduto)}
            </div>
        )
    }
}

export default Categoria