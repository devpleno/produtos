import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Home from './Home'
import Produtos from './Produtos'
import Sobre from './Sobre'

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      produtos: [],
      categorias: [],
      categoriaAtual: null
    }

    this.loadCategorias = this.loadCategorias.bind(this)
    this.removeCategoria = this.removeCategoria.bind(this)
    this.createCategoria = this.createCategoria.bind(this)
    this.editCategoria = this.editCategoria.bind(this)

    this.createProduto = this.createProduto.bind(this)
    this.loadProdutosCategorias = this.loadProdutosCategorias.bind(this)
    this.loadCategoriaAtual = this.loadCategoriaAtual.bind(this)
    this.removeProduto = this.removeProduto.bind(this)
    this.readProdutos = this.readProdutos.bind(this)
    this.editProduto = this.editProduto.bind(this)
  }

  loadCategorias() {
    this.props.api.loadCategorias().then(res => {
      this.setState({ categorias: res.data })
    });
  }

  removeCategoria(cat) {
    this.props.api.deleteCategorias(cat.id).then(res => {
      this.loadCategorias();
    });
  }

  createCategoria(categoria) {
    this.props.api.newCategoria(categoria).then(res => {
      this.loadCategorias();
    });
  }

  editCategoria(categoria) {
    this.props.api.editCategoria(categoria).then(res => {
      this.loadCategorias();
    });
  }

  createProduto(produto) {
    return this.props.api.newProduto(produto);
  }

  removeProduto(prod) {
    this.props.api.deleteProduto(prod.id).then(res => {
      this.loadProdutosCategorias(prod.categoria);
    });
  }

  loadProdutosCategorias(catID) {
    this.props.api.loadProdutosCategorias(catID).then(res => {
      this.setState({ produtos: res.data })
    });
  }

  loadCategoriaAtual(catID) {
    this.props.api.loadCategoriaAtual(catID).then(res => {
      this.setState({ categoriaAtual: res.data })
    });
  }

  readProdutos(id) {
    return this.props.api.readProdutos(id)
  }

  editProduto(produto) {
    return this.props.api.editProduto(produto)
  }

  render() {
    return (
      <Router>

        <div>
          <nav className="navbar navbar-inverse">
            <div className="container">
              <div className="navbar-header">
                <a href='/' className="navbar-brand">
                  Gerenciador de Produtos
            </a>
              </div>

              <ul className="nav navbar-nav">
                <li><Link to='/'>Home</Link></li>
                <li><Link to='/produtos'>Produtos</Link></li>
                <li><Link to='/sobre'>Sobre</Link></li>
              </ul>
            </div>
          </nav>

          <div className="container">
            <Route exact path="/" component={Home} />
            <Route exact path="/sobre" component={Sobre} />

            <Route path="/produtos" render={(props) => {
              return <Produtos {...props} 
                        loadCategorias={this.loadCategorias} 
                        editCategoria={this.editCategoria} 
                        removeCategoria={this.removeCategoria}  
                        categorias={this.state.categorias}
                        createCategoria={this.createCategoria} 

                        produtos={this.state.produtos}
                        readProdutos={this.readProdutos}
                        createProduto={this.createProduto} 
                        editProduto={this.editProduto} 
                        categoriaAtual={this.state.categoriaAtual} 
                        removeProduto={this.removeProduto} 
                        loadCategoriaAtual={this.loadCategoriaAtual} 
                        loadProdutosCategorias={this.loadProdutosCategorias} />
            }} />
          </div>
        </div>

      </Router>
    );
  }
}

export default App;
