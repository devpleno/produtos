import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'

class ProdutosEditar extends Component {

    constructor(props) {
        super(props)

        this.state = {
            redirect: false
        }

        this.handleEditProduto = this.handleEditProduto.bind(this)
    }
    componentDidMount() {
        this.props.readProdutos(this.props.match.params.id).then((res) => {
            this.refs.produto.value = res.data.produto
            this.refs.categoria.value = res.data.categoria
        })
    }

    handleEditProduto() {
        const produto = {
            id: this.props.match.params.id,
            produto: this.refs.produto.value,
            categoria: this.refs.categoria.value
        }

        this.props.editProduto(produto).then((res) => {
            this.setState({
                redirect: '/produtos/categoria/' + produto.categoria
            })
        })
    }

    render() {
        const { categorias } = this.props

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }

        return (
            <div>
                <h2>Tela de edição de produto</h2>

                <select ref="categoria">
                    {categorias.map((cat) => {
                        return <option key={cat.id} value={cat.id}>{cat.categoria}</option>
                    })}
                </select>

                <input ref="produto" className="form-control" placeholder="Informe o nome" />

                <button className="btn btn-success" onClick={this.handleEditProduto}>Salvar</button>
            </div>
        )
    }
}

export default ProdutosEditar