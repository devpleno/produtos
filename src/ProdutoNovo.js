import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'

class ProdutoNovo extends Component {

    constructor(props){
        super(props)

        this.state = {
            redirect: false
        }

        this.handleNewProduto = this.handleNewProduto.bind(this)
    }

    handleNewProduto() {
        const produto = {
            produto: this.refs.produto.value,
            categoria: this.refs.categoria.value
        }

        this.props.createProduto(produto).then((res) => {
            this.setState({
                redirect: '/produtos/categoria/' + produto.categoria
            })
        })
    }

    render() {
        const { categorias } = this.props;

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }

        return (
            <div>
                <h2>Novo produto</h2>

                <select ref="categoria">
                    {categorias.map((cat) => {
                        return <option key={cat.id} value={cat.id}>{cat.categoria}</option>
                    })}
                </select>

                <input ref="produto" className="form-control" placeholder="Preencha aqui" />

                <button className="btn btn-success" onClick={this.handleNewProduto}>Salvar</button>
            </div>
        )
    }
}

export default ProdutoNovo