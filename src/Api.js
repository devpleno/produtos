import axios from 'axios'

const api = axios.create({
    baseURL: 'http://localhost:3001/'
})

const apis = {
    loadCategorias: () => api.get('categorias'),
    loadCategoriaAtual: (id) => api.get('categorias/' + id),
    newCategoria: (params) => api.post('categorias', params),
    editCategoria: (params) => api.put('categorias/' + params.id, params),
    deleteCategorias: (id) => api.delete('categorias/' + id),
    
    loadProdutosCategorias: (id) => api.get('produtos?categoria=' + id),
    editProduto: (params) => api.put('produtos/' + params.id, params),
    deleteProduto: (id) => api.delete('produtos/' + id),
    readProdutos: (id) => api.get('produtos/' + id),
    newProduto: (params) => api.post('produtos', params)
}

export default apis