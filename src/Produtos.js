import React, { Component } from 'react'

import { Route, Link } from 'react-router-dom'
import ProdutosHome from './ProdutosHome'
import Categoria from './Categoria'
import ProdutoNovo from './ProdutoNovo'
import ProdutosEditar from './ProdutosEditar'

class Produtos extends Component {

    constructor(props) {
        super(props)

        this.state = {
            editingCategoria: ''
        }

        this.handleNewCategoria = this.handleNewCategoria.bind(this)
        this.renderCategoria = this.renderCategoria.bind(this)
        this.editCategoria = this.editCategoria.bind(this)
        this.cancelEditing = this.cancelEditing.bind(this)
        this.handleEditCategoria = this.handleEditCategoria.bind(this)
    }

    componentDidMount() {
        this.props.loadCategorias();
    }

    editCategoria(cat) {
        this.setState({
            editingCategoria: cat.id
        })
    }

    cancelEditing() {
        this.setState({
            editingCategoria: ''
        })
    }

    renderCategoria(cat) {
        return (
            <li key={cat.id}>
                {this.state.editingCategoria === cat.id &&
                    <div className="input-group">

                        <div className="input-group-btn">
                            <input type="text" ref={'cat-' + cat.id} className="form-control" defaultValue={cat.categoria} onKeyUp={this.handleEditCategoria} />

                            <button className="btn btn-danger" onClick={() => this.cancelEditing(cat)}>
                                Cancelar
                            </button>
                        </div>

                    </div>
                }

                {this.state.editingCategoria !== cat.id &&
                    <div>
                        <button className="btn btn-danger" onClick={() => this.props.removeCategoria(cat)}>
                            <i className="glyphicon glyphicon-remove"></i>
                        </button>

                        <button className="btn btn-warning" onClick={() => this.editCategoria(cat)}>
                            <i className="glyphicon glyphicon-pencil"></i>
                        </button>

                        <Link to={`/produtos/categoria/${cat.id}`}>{cat.categoria}</Link>
                    </div>
                }
            </li>
        )
    }

    handleEditCategoria(e) {
        if (e.keyCode === 13) {
            this.props.editCategoria({
                id: this.state.editingCategoria,
                categoria: this.refs['cat-' + this.state.editingCategoria].value
            });

            this.cancelEditing();
        }
    }

    handleNewCategoria(e) {
        if (e.keyCode === 13) {
            this.props.createCategoria({
                categoria: this.refs.categoria.value
            });

            this.refs.categoria.value = '';
        }
    }

    render() {
        const { match, categorias, produtos } = this.props

        return (
            <div className="row">
                <div className="col-sm-3 col-md-3 col-lg-3">
                    <h3>Categorias</h3>

                    <ul style={{ listStyle: 'none', padding: 0 }}></ul>

                    <ul>
                        {categorias.map(this.renderCategoria)}
                    </ul>

                    <div className="alert">
                        <input type="text" className="form-control" ref='categoria' placeholder="Nova categoria" onKeyUp={this.handleNewCategoria} />
                    </div>

                    <Link to='/produtos/novo'>Novo produto</Link>
                </div>

                <div className="col-sm-9 col-md-9 col-lg-9">
                    <h1>Produtos</h1>
                    <Route exact path={match.url} component={ProdutosHome} />

                    <Route exact path={match.url + '/novo'} render={(props) => {
                            return <ProdutoNovo {...props} categorias={categorias} createProduto={this.props.createProduto} />
                    }} />

                    <Route path={match.url + '/categoria/:catID'} render={(props) => {
                            return <Categoria {...props} produtos={produtos} removeProduto={this.props.removeProduto} loadProdutosCategorias={this.props.loadProdutosCategorias} categoriaAtual={this.props.categoriaAtual} loadCategoriaAtual={this.props.loadCategoriaAtual} />
                    }} />

                    <Route path={match.url + '/editar/:id'} render={(props) => {
                            return <ProdutosEditar {...props} categorias={categorias} readProdutos={this.props.readProdutos} editProduto={this.props.editProduto} />
                    }} />

                </div>
            </div>
        )
    }
}

export default Produtos